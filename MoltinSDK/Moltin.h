//
//  Moltin.h
//  MoltinSDK
//
//  Created by Jan Zimandl on 20.04.15.
//  Copyright (c) 2015 STRV. All rights reserved.
//




//clientId: zNo5UZRbbhUZ5AskL8ISiSIONzjVM2krVZ8x04UJ
//clientSecret: LbiVK9MOAS4k2vZ5f2QxG4GEHi0PGXKK0adYk3fE

#import <Foundation/Foundation.h>

@interface Moltin : NSObject

@property (nonatomic, strong) NSString *clientId;
@property (nonatomic, strong) NSString *clientSecret;
@property (nonatomic, strong) NSString *mtoken;
@property (nonatomic, strong) NSDate *mexpires;


#pragma mark - Setup

+ (void)setupWithClientId:(NSString *)clientId andClientSecret:(NSString *)clientSecret;
+ (void)setupWithClientId:(NSString *)clientId;

#pragma mark - Shared Instance

+ (instancetype)sharedInstance;




#pragma mark - REQUESTS


#pragma mark AUTHENTICATION

+ (void)authenticateWithCompletionBlock:(void (^)(NSError *error))completion;
- (void)authenticateWithCompletionBlock:(void (^)(NSError *error))completion;



#pragma mark PRODUCT

+ (void)productGetWithId:(NSInteger)productId andCompletionBlock:(void (^)(NSDictionary *product, NSError *error))completion;
- (void)productGetWithId:(NSInteger)productId andCompletionBlock:(void (^)(NSDictionary *product, NSError *error))completion;
+ (void)productListWithTerms:(NSDictionary *)terms andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion;
- (void)productListWithTerms:(NSDictionary *)terms andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion;
+ (void)productSearchWithTerms:(NSDictionary *)terms andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion;
- (void)productSearchWithTerms:(NSDictionary *)terms andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion;

@end
