//
//  MoltinSDK.h
//  MoltinSDK
//
//  Created by Jan Zimandl on 20.04.15.
//  Copyright (c) 2015 STRV. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for MoltinSDK.
FOUNDATION_EXPORT double MoltinSDKVersionNumber;

//! Project version string for MoltinSDK.
FOUNDATION_EXPORT const unsigned char MoltinSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MoltinSDK/PublicHeader.h>


#import <MoltinSDK/Moltin.h>