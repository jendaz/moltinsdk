//
//  Moltin.m
//  MoltinSDK
//
//  Created by Jan Zimandl on 20.04.15.
//  Copyright (c) 2015 STRV. All rights reserved.
//

#import "Moltin.h"
#import <AFNetworking/AFNetworking.h>

#define MOLTIN_API_URL @"https://api.molt.in"
#define MOLTIN_API_OAUTH_PATH @"oauth/access_token"
#define MOLTIN_API_VERSION @"v1"

@implementation Moltin



+ (void)setupWithClientId:(NSString *)cId
{
    Moltin *m = [self sharedInstance];
    [m setClientId:cId];
    [m setClientSecret:nil];
}

+ (void)setupWithClientId:(NSString *)cId andClientSecret:(NSString *)cSec
{
    Moltin *m = [self sharedInstance];
    [m setClientId:cId];
    [m setClientSecret:cSec];
}

+ (instancetype)sharedInstance
{
    static Moltin *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}



#pragma mark - Helpers


- (NSString *)_uriParamsFromDict:(NSDictionary *)dict
{
    if (!dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        return nil;
    }
    
    NSMutableString *resultS = [NSMutableString string];
    for (NSString *key in [dict allKeys])
    {
        if ([resultS length]!=0)
        {
            [resultS appendString:@"&"];
        }
        [resultS appendFormat:@"%@=%@", key, dict[key]];
    }
    
    if ([resultS length]==0)
    {
        return nil;
    }
    return resultS;
}

- (void)_requestWithUri:(NSString *)uri
                 method:(NSString *)method
                   data:(NSDictionary *)data
               callback:(void (^)(id result, NSError *error))callback
{
    NSURL *url = [NSURL URLWithString:MOLTIN_API_URL];
    url = [url URLByAppendingPathComponent:MOLTIN_API_VERSION];
    url = [url URLByAppendingPathComponent:uri];
    
    NSString *dataString = [self _uriParamsFromDict:data];

    if ([method isEqualToString:@"GET"] && dataString)
    {
        url = [url URLByAppendingPathComponent:@"?"];
        url = [url URLByAppendingPathComponent:dataString];
    }
    
    //NSLog(@"MTLN request url: %@", url);
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
    
    NSString *tokenHeader = [NSString stringWithFormat:@"Bearer %@", _mtoken];
    [req addValue:tokenHeader forHTTPHeaderField:@"Authorization"];
    
    [req setHTTPMethod:method];
    
    if ([method isEqualToString:@"POST"] && dataString)
    {
        [req setHTTPBody:[dataString dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:req];
    [op setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *dict = (NSDictionary *)responseObject;
         if ([dict[@"status"] boolValue])
         {
             id result = dict[@"result"];
             
             callback(result, nil);
         }else{
             NSString *errS = dict[@"error"];
             NSError *err = [NSError errorWithDomain:@"MTLN ERROR"
                                                code:-1
                                            userInfo:@{
                                                       NSLocalizedDescriptionKey:errS,
                                                       NSURLErrorKey:req.URL,
                                                       }];
             callback(nil, err);
         }
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         callback(nil, error);
     }];
    [op start];
    
    /*
    [NSURLConnection sendAsynchronousRequest:req
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (connectionError)
         {
             callback(nil, connectionError);
             return;
         }
         
         NSError *err;
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
         if (err)
         {
             callback(nil, err);
             return;
         }
         
         if ([dict[@"status"] boolValue])
         {
             id result = dict[@"result"];
             
             callback(result, nil);
         }else{
             NSString *errS = dict[@"error"];
             NSError *err = [NSError errorWithDomain:@"MTLN ERROR"
                                                code:-1
                                            userInfo:@{
                                                       NSLocalizedDescriptionKey:errS,
                                                       NSURLErrorKey:req.URL,
                                                       }];
             callback(nil, err);
         }
         
         
     }];
     */
}




#pragma mark - REQUESTS


#pragma mark AUTHENTICATION


+ (void)authenticateWithCompletionBlock:(void (^)(NSError *))completion
{
    [[self sharedInstance] authenticateWithCompletionBlock:completion];
}

- (void)authenticateWithCompletionBlock:(void (^)(NSError *))completion
{
    NSString *postString;
    
    if (_clientId && _clientSecret)
    {
        postString = [self _uriParamsFromDict:@{
                                                @"client_id": _clientId,
                                                @"client_secret": _clientSecret,
                                                @"grant_type": @"client_credentials",
                                                }];
    }
    else if (_clientId)
    {
        postString = [self _uriParamsFromDict:@{
                                                @"client_id": _clientId,
                                                @"grant_type": @"implicit",
                                                }];
    }
    else
    {
        completion([NSError errorWithDomain:@"auth" code:1234 userInfo:nil]);
        return;
    }
    
    NSURL *url = [NSURL URLWithString:MOLTIN_API_URL];
    url = [url URLByAppendingPathComponent:MOLTIN_API_OAUTH_PATH];
    
    //NSLog(@"MTLN auth url: %@", url);
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc] initWithURL:url];
    [req setHTTPMethod:@"POST"];
    [req setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:req];
    [op setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *dict = (NSDictionary *)responseObject;
         
         _mtoken = dict[@"access_token"];
         _mexpires = [NSDate dateWithTimeIntervalSince1970:[dict[@"expires"] longLongValue]];
         
         completion(nil);
     }
                              failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         completion(error);
     }];
    [op start];
    
    /*
    [NSURLConnection sendAsynchronousRequest:req
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError)
     {
         if (connectionError)
         {
             completion(connectionError);
             return;
         }
         
         NSError *err;
         NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&err];
         if (err)
         {
             completion(err);
             return;
         }
         
         _mtoken = dict[@"access_token"];
         _mexpires = [NSDate dateWithTimeIntervalSince1970:[dict[@"expires"] longLongValue]];
         
         completion(connectionError);
     }];
     */
}



#pragma mark PRODUCT


+ (void)productGetWithId:(NSInteger)productId
      andCompletionBlock:(void (^)(NSDictionary *product, NSError *error))completion
{
    [[self sharedInstance] productGetWithId:productId
                         andCompletionBlock:completion];
}

- (void)productGetWithId:(NSInteger)productId
      andCompletionBlock:(void (^)(NSDictionary *product, NSError *error))completion
{
    [self _requestWithUri:[NSString stringWithFormat:@"products/%ld", (long)productId]
                   method:@"GET"
                     data:nil
                 callback:^(id result, NSError *error)
     {
         //NSLog(@"MTLN product get result: %@", result);
         
         if (error)
         {
             completion(nil, error);
         }
         else if ([result isKindOfClass:[NSDictionary class]])
         {
             completion(result, nil);
         }
         else
         {
             completion(nil, nil);
         }
         
     }];
}


+ (void)productListWithTerms:(NSDictionary *)terms
          andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion
{
    [[self sharedInstance] productListWithTerms:terms
                             andCompletionBlock:completion];
}

- (void)productListWithTerms:(NSDictionary *)terms
          andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion;
{
    [self _requestWithUri:@"products"
                   method:@"GET"
                     data:terms
                 callback:^(id result, NSError *error)
     {
         
         if (error)
         {
             completion(nil, error);
         }
         else if ([result isKindOfClass:[NSArray class]])
         {
             completion(result, nil);
         }
         else
         {
             completion(nil, nil);
         }
         
     }];
}


+ (void)productSearchWithTerms:(NSDictionary *)terms
            andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion
{
    [[self sharedInstance] productSearchWithTerms:terms
                               andCompletionBlock:completion];
}

- (void)productSearchWithTerms:(NSDictionary *)terms
            andCompletionBlock:(void (^)(NSArray *array, NSError *error))completion;
{
    [self _requestWithUri:@"products/search"
                   method:@"GET"
                     data:terms
                 callback:^(id result, NSError *error)
     {
         
         if (error)
         {
             completion(nil, error);
         }
         else if ([result isKindOfClass:[NSArray class]])
         {
             completion(result, nil);
         }
         else
         {
             completion(nil, nil);
         }
         
     }];
}









@end
